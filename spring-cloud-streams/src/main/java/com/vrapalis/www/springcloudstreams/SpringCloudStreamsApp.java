package com.vrapalis.www.springcloudstreams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.function.Consumer;

@SpringBootApplication
public class SpringCloudStreamsApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudStreamsApp.class, args);
    }

    @Bean
    public Consumer<String> consumer() {
        return msg -> System.out.println(msg);
    }
}
